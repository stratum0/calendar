#!/bin/bash
set -e

python -m coverage run -m unittest
python -m coverage report
pycodestyle --ignore=E501,W191,E128,E117,E126 *.py tests/*.py
