# -*- coding: utf8 -.-
import unittest
import calendargenerator as cg
import datetime


class TestUrls(unittest.TestCase):
    def setUp(self):
        pass

    def test_internUrlTitle(self):
        url_date = cg.SingleDate("abc [[test_url|test_title]]", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getURL(), "https://stratum0.org/wiki/test_url")
        self.assertEqual(url_date.getPlainName(), "abc test_title")

    def test_internUrl(self):
        url_date = cg.SingleDate("abc [[test_url]]", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getURL(), "https://stratum0.org/wiki/test_url")
        self.assertEqual(url_date.getPlainName(), "abc test_url")

    def test_internUmlautUrl(self):
        url_date = cg.SingleDate(u"[[test_ürl]]", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getURL(), u"https://stratum0.org/wiki/test_%C3%BCrl")
        self.assertEqual(url_date.getPlainName(), u"test_ürl")

        url_date2 = cg.SingleDate(u"[[test_ürlä]]", "cat", [20, 9, 2014])
        self.assertEqual(url_date2.getURL(), u"https://stratum0.org/wiki/test_%C3%BCrl%C3%A4")
        self.assertEqual(url_date2.getPlainName(), u"test_ürlä")

    def test_externUrl(self):
        url_date = cg.SingleDate("abc [https://stratum0.net/ title]", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getURL(), "https://stratum0.net/")
        self.assertEqual(url_date.getPlainName(), "abc title")

    def test_multiInternalURL(self):
        url_date = cg.SingleDate("[[Vorträge]] ([[Vorträge/Vorbei|Aufzeichnungen]])", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "Vorträge (Aufzeichnungen)")
        self.assertEqual(url_date.getURL(), "https://stratum0.org/wiki/Vortr%C3%A4ge")

    def test_multiExternalURL(self):
        url_date = cg.SingleDate("[https://events.ccc.de/congress/2014/ 31C3] ([https://events.ccc.de/congress/2014/wiki/Assembly:Stratum_0 Assembly])", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "31C3 (Assembly)")
        self.assertEqual(url_date.getURL(), "https://events.ccc.de/congress/2014/")

    def test_firstUrl(self):
        url_date = cg.SingleDate("abc [https://stratum0.net/ title] [[test_url|test_title]]", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getURL(), "https://stratum0.net/")
        self.assertEqual(url_date.getPlainName(), "abc title test_title")

        url_date2 = cg.SingleDate("abc [[test_url|test_title]] [https://stratum0.net/ title]", "cat", [20, 9, 2014])
        self.assertEqual(url_date2.getURL(), "https://stratum0.org/wiki/test_url")
        self.assertEqual(url_date2.getPlainName(), "abc test_title title")


class TestPlainName(unittest.TestCase):
    def setUp(self):
        pass

    def test_emph(self):
        url_date = cg.SingleDate("abc ''def''", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "abc def")

    def test_double_emph(self):
        url_date = cg.SingleDate("abc ''def'' ''ghi''", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "abc def ghi")

    def test_bold_emph(self):
        url_date = cg.SingleDate("abc '''def''' ''ghi''", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "abc def ghi")

    def test_bold(self):
        url_date = cg.SingleDate("abc '''def'''", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "abc def")

    def test_double_bold(self):
        url_date = cg.SingleDate("abc '''def''' '''ghi'''", "cat", [20, 9, 2014])
        self.assertEqual(url_date.getPlainName(), "abc def ghi")


class TestWikiParser(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None
        pass

    def test_parseWiki(self):
        now = cg.tz.localize(datetime.datetime(2014, 10, 10, 10, 10))
        with open("tests/wiki/general.wiki") as f:
            result = cg.parse_wiki_page(f.read())
        for entry in result:
            if isinstance(entry, cg.Generator):
                for subentry in entry.entries:
                    self.assertLessEqual(subentry.start_datetime(), subentry.end_datetime())
            else:
                self.assertLessEqual(entry.start_datetime(), entry.end_datetime())

        output = cg.generate_wiki_section(cg.expand_dates(result), "templates/termine_haupt.de.wiki", cg.LANG_DE, now=now)
        expected_output = """<!-- automatisch generierter Content fängt hier an -->
=== '''Aktuelles''' ===
<!--
  Achtung:
  Dies ist ein automatisch generierter Abschnitt.  Alle Änderungen werden
  überschrieben.  Wenn du Termine eintragen willst, bearbeite bitte
  https://stratum0.org/wiki/Termine
-->
----
''siehe auch [[Kalender]] <div style="float:right">[[Termine| Termine verwalten]] (beta)</div>''
* So, 12.10. 08:00 – 18:00: [[Wurstworkshop]] (Küche)
* Di, 14.10. 19:00: [[Vegan Academy]]
* Di, 28.10. 19:00: [[Vegan Academy]]
* Di, 04.11. 08:00 bis Mi, 05.11. 12:00: [[Wurstworkshop]] (Küche)
----

==== Neulich: ====
<!--
  Achtung:
  Dies ist ein automatisch generierter Abschnitt.  Alle Änderungen werden
  überschrieben.  Wenn du Termine eintragen willst, bearbeite bitte
  https://stratum0.org/wiki/Termine
-->
''siehe auch [[Timeline]]''
* Di, 30.09. 19:00: [[Vegan Academy]]
* So, 14.09. 19:00: [[Vorträge]]
<!-- automatisch generierter Content hört hier auf -->"""
        self.assertEqual(output, expected_output)
        cg.generate_ical(result, "/dev/null")
        self.assertEqual(len(result), 15)

    def test_parseWiki_wrongWeekday(self):
        now = cg.tz.localize(datetime.datetime(2014, 10, 10, 10, 10))
        with open("tests/wiki/wrong_weekday.wiki") as f:
            with self.assertRaises(cg.InvalidDateEntryException):
                cg.parse_wiki_page(f.read())

    def test_parseWiki_invalidTime(self):
        now = cg.tz.localize(datetime.datetime(2014, 10, 10, 10, 10))
        with open("tests/wiki/invalid_time.wiki") as f:
            with self.assertRaises(ValueError):
                cg.parse_wiki_page(f.read())

    def test_parseWiki_wrongTime(self):
        now = cg.tz.localize(datetime.datetime(2014, 10, 10, 10, 10))
        with open("tests/wiki/wrong_time.wiki") as f:
            with self.assertRaises(cg.InvalidDateEntryException):
                cg.parse_wiki_page(f.read())

    def test_archiveWiki(self):
        now = cg.tz.localize(datetime.datetime(2014, 10, 10, 10, 10))
        with open("tests/wiki/general.wiki") as f:
            wiki_text = f.read()
        with open("tests/wiki/archive.wiki") as f:
            archive_text = f.read()

        new_wiki_text, new_archive_text, n = cg.move_to_archive(wiki_text, archive_text, now)
        self.assertEqual(n, 12)

        new2_wiki_text, new2_archive_text, n2 = cg.move_to_archive(new_wiki_text, new_archive_text, now)
        self.assertEqual(new_wiki_text, new2_wiki_text)
        self.assertEqual(new_archive_text, new2_archive_text)
        self.assertEqual(n2, 0)

        new3_wiki_text, new3_archive_text, n3 = cg.move_to_archive(new2_wiki_text, new2_archive_text, now)
        self.assertEqual(n3, 0)
        self.assertEqual(new_wiki_text, new3_wiki_text)
        self.assertEqual(new_archive_text, new3_archive_text)
