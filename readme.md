# stratum0-calendar
stratum0-calendar generates multiple calendar/events representations for [stratum0.org](https://stratum0.org)

* ical(ics)-file
* html-calendar using [bootstrap-calendar](https://github.com/Serhioromano/bootstrap-calendar)
* wiki-pages in multiple languages

## Installation
0. Be sure to use Python 3. If in doubt, set up a [virtualenv][] with `$ virtualenv -p python3 venv; . venv/bin/activate`
1. `$ pip install -r requirements.txt`
2. setup `config.py` (see `config.py.example` for an example)
3. run `$ ./bot.py`

[virtualenv]: https://virtualenv.pypa.io/en/stable/

### Tests
To run the tests, do a `pip install -r requirements-tests.txt`, followed by a `python -m nose`.

## Supported Date-Formats

* Event
	* over the whole day `DD.MM.YYYY`
	* at a given time of a day `DD.MM.YYYY hh.mm`
	* at a given time of a day with specific end `DD.MM.YYYY hh.mm-hh.mm`
* Multiple day event
	* `DD.MM.YYYY - DD.MM.YYYY`
	* with start and end time `DD.MM.YYYY hh.mm - DD.MM.YYYY hh.mm`
* Repeating event
	* every week `wd, hh.mm` plus `DD.MM.YYYY - DD.MM.YYYY`
	* every week with end time `wd, hh.mm - hh.mm` plus `DD.MM.YYYY - DD.MM.YYYY`
	* instead of `wd` it is possible to use `wd/n` to generate an event every n-th week

# License

This software is provided under the MIT License, as given below:

Copyright (c) 2014-2024 Roland Hieber, Sebastian Willenborg, and contributors

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next
paragraph) shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
